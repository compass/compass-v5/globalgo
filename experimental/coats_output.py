import math
from time import process_time

from globalgo.core.utils import as_tuple, F
from globalgo.core import (
    Newton,
    SimpleNewtonNorm,
    TimeTick,
    Timeloop,
    Timestepper,
    EventPool,
)
from globalgo.timestep import DynamicStep, FixedStep
from globalgo.callbacks import ListLog, with_callbacks


class ScalarLinearSolver:
    def set_up(self, A):
        self.A = A

    def solve(self, B):
        status = 0
        return B / self.A, status


class ExpAssembler:
    def __init__(self, tau):
        self.tau = tau

    def set_up(self, solution, dt, time):
        self.dt = dt
        self.time = time
        self.prev_solution = solution

    def evaluate(self, solution):
        c = 1 + self.tau * self.dt
        return c * solution - self.prev_solution, c

    def update_solution(self, solution, dx):
        return solution - dx


sec_to_year = lambda s: s / (60 * 60 * 24 * 365)


class MyCallbacks:
    def __init__(self, log=print):
        self.log = log

    def timeloop_run(self, solution, stop, start=None, events=None):
        self.start = TimeTick.from_time(start, 0)
        self.stop = TimeTick.from_time(stop, math.inf)
        cpu_start_time = process_time()
        yield
        cpu_elapsed = process_time() - cpu_start_time
        self.log(
            f"Elapsed computing time (including output): {cpu_elapsed:.4g} seconds"
        )

    def timestepper_make_step(self, prev_solution, steps, tick):
        if math.isfinite(self.stop.time):
            progess = (tick.time - self.start.time) / (self.stop.time - self.start.time)
            msg_final_time = (
                f" -> {self.stop.time:10.4g} s"
                f" = {sec_to_year(self.stop.time):10.4g} ({progess:.2f}% done)"
            )
        else:
            progess = (tick.iteration - self.start.iteration) / (
                self.stop.iteration - self.start.iteration
            )
            msg_final_time = (
                f" -> iteration {self.stop.iteration:6d} ({progess:.2f}% done)"
            )
        self.log(f"\n{f'** Time Step (iteration): {tick.iteration+1} ':*<78}")
        self.log(
            f"Current time: {tick.time:10.4g} s = {sec_to_year(tick.time):10.4g} y"
            + msg_final_time
        )
        solution, dt, status = yield
        self.log(f"Success with timestep: {dt:10.4g} s = {sec_to_year(dt):10.4g} y")
        self.log(f"solution variation: {solution-prev_solution:.4g}")

    def timestepper_try_step(self, iteration, solution, dt, tick):
        solution, status = yield

    def newton_solve(self, first_guess, dt, time):
        self.log(
            f"trying newton with timestep: {dt:10.4g} s = {sec_to_year(dt):10.4g} y"
        )
        solution, status = yield
        *status, last_status = status
        self.log(
            f"Total Newton iteration: {len(status)} ; "
            f"linear solver statuses : {[s.ls_status for s in status]}"
        )

    def newton_step(self, iteration, solution, dx, status_history):
        self.newton_iteration = iteration - 1
        converged, solution, dx = yield

    def residual_norm_set_up(self, first_guess, assembler):
        set_up_info = yield
        # set_up_info is residual_norm dependent
        self.log(f"timestep residual scale: {set_up_info=}")

    def residual_norm_norm(self, solution, residual, dx):
        norm = yield
        norm, *info = as_tuple(norm)
        self.log(
            f"Newton {self.newton_iteration:>3}  "
            f"residuals: "
            f"{info=}  "
            f"relative (conv. crit.) = {norm:10.4g}"
        )

    def assembler_set_up(self, solution, dt, time):
        set_up_info = yield
        # set_up_info is assembler dependent

    def assembler_evaluate(self, solution):
        residual, jacobian = yield
        # dump linear system
        # could need newton_iteration and time.tick

    def assembler_update_solution(self, prev_solution, dx):
        solution = yield

    def linear_solver_set_up(self, A):
        set_up_info = yield
        # set_up_info is linear_solver dependent

    def linear_solver_solve(self, b):
        try:
            x, status = yield
        except Exception as err:
            # Here, you could dump linear system on error
            # or just save the exception for later processing
            self.linear_solver_error = err
            raise
        else:
            self.linear_solver_error = None


initial_solution = 1
tau = 1
final_time = 5
dt = 5e-1

cb = MyCallbacks()

loop = with_callbacks(cb.timeloop_run)("run")(
    Timeloop(
        F(
            with_callbacks(cb.timestepper_make_step)("make_step"),
            with_callbacks(cb.timestepper_try_step)("try_step"),
        )(
            Timestepper(
                F(
                    with_callbacks(cb.newton_solve)("solve"),
                    with_callbacks(cb.newton_step)("step"),
                )(
                    Newton(
                        assembler=F(
                            with_callbacks(cb.assembler_evaluate)("evaluate"),
                            with_callbacks(cb.assembler_set_up)("set_up"),
                            with_callbacks(cb.assembler_update_solution)(
                                "update_solution"
                            ),
                        )(ExpAssembler(tau)),
                        residual_norm=F(
                            with_callbacks(cb.residual_norm_norm)("norm"),
                            with_callbacks(cb.residual_norm_set_up)("set_up"),
                        )(SimpleNewtonNorm()),
                        linear_solver=F(
                            with_callbacks(cb.linear_solver_solve)("solve"),
                            with_callbacks(cb.linear_solver_set_up)("set_up"),
                        )(ScalarLinearSolver()),
                        tolerance=1e-8,
                        maxiter=10,
                    ),
                ),
            ),
        ),
        # DynamicStep(dt),
        FixedStep(dt),
    ),
)
solution = loop.run(
    solution=initial_solution,
    stop=final_time,
    # stop=TimeTick(None, 3),
    # events=EventPool().add_recurring_time(
    #     period=0.1,
    #     action=time_log,
    # ),
)
