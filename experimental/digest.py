def test_coats():
    config = {
        "newton": {
            "tolerance": 1e-8,
            "maxiter": 30,
        },
        "linear_solver": {
            "__type__": "petsc.gmres",
            "param_1": ...,
        },
        "timesteps": {
            "__type__": "DynamicStep",
            "initial_timestep": 1.23,
        },
        "callbacks": [
            {
                "__type__": "default",
                "log": {
                    "__type__": "print_and_log",
                    "filename": "output.txt",
                },
            },
        ],
        "events": [
            {
                "__type__": "default",
                "period": 24 * 60 * 60,
            },
        ],
    }
    """# yaml
    newton:
      tolerance: 1e-8
      maxiter: 30
    linear_solver:
      __type__: petsc.gmres
      param_1: ...
    timesteps:
      __type__: DynamicStep
      initial_timestep: 1.23
    callbacks:
      - __type__: default
        log:
          __type__: print_and_log
          filename: output.txt
    events:
      - __type__: default
        period: 24 * 60 * 60
    """
