import verstr

try:
    from . import _version

    __version__ = verstr.verstr(_version.version)
except ImportError:
    __version__ = None


from . import core
from . import callbacks
from . import timestep
from .config import configure_timeloop
