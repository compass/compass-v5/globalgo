from .core import recurring
from .timestep import DynamicStep, FixedStep


NAMESPACES = {
    "timesteps": {
        "DynamicStep": DynamicStep,
        "FixedStep": FixedStep,
    },
    "linear_solver": {
        "petsc.gmres": lambda **kwds: ("here is gmres", kwds),
        "eigen.lu": lambda **kwds: ("here is lu", kwds),
    },
    "callbacks": {
        "print": lambda: print,
        "trace": lambda: lambda **k: None,
    },
    "events": {
        "recurring": recurring,
    },
}


def get_namespaces():
    # TODO: add plugin feature
    #    e.g. entry points:
    #    `{"globalgo.namespace.timesteps": ["AdaptiveStep: mymodule:AdaptiveStep"]}`
    return NAMESPACES
