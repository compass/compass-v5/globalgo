import time
import io
import math
import statistics

__all__ = ["Timer"]


def make_table(data, columns, title, index, width):
    name_width = max(len(n) for n in data) + 1
    name_width = max(width, name_width)
    bar_str = f"{'-'*(len(columns) * (width + 3) + name_width)}\n"
    s = io.StringIO()
    if title:
        s.write(title.strip())
        s.write("\n")
    s.write(index.rjust(name_width))
    for c in columns:
        s.write(" | " + c.rjust(width))
    s.write("\n" + bar_str)
    for name, values in data.items():
        s.write(name.rjust(name_width))
        for c, func in columns.items():
            try:
                value = func(values)
            except Exception:
                value = "NA"
            s.write(f" | {value.rjust(width)}")
        s.write("\n")
    s.write(bar_str)
    return s.getvalue()


def report_summary(data, title=None, index="method", width=11):
    cols = {
        "#calls": lambda l: f"{len(l):_d}",
        "cumul.": lambda l: f"{sum(l):g}",
        "mean": lambda l: f"{statistics.mean(l):g}",
        "stdev": lambda l: f"{statistics.stdev(l) if len(l) > 1 else 0:g}",
        "median": lambda l: f"{statistics.median(l):g}",
        "min": lambda l: f"{min(l):g}",
        "max": lambda l: f"{max(l):g}",
    }
    return make_table(data, cols, title, index, width)


def report_groups(data, title=None, index="method", width=11):
    cols = {
        "cumul.": lambda v: f"{v:g}",
        "% cumul.": lambda v: f"{v/tot:.2%}",
    }
    tot = sum(data.values())
    return make_table(data, cols, title, index, width)


def get_callees(data):
    all_callees = {}
    data = sorted(data, key=lambda v: v[1])
    for name, start, stop in data:
        if name not in all_callees:
            all_callees[name] = set()
        for other, a, b in data:
            if b < start:
                continue
            if start < a < b < stop:
                all_callees[name].add(other)
            if a > stop:
                break
    callees = {k: [] for k in all_callees}
    for callee in all_callees:
        candidates = {n for n, v in all_callees.items() if callee in v}
        if not candidates:
            continue
        for caller in candidates:
            if all_callees[caller] & candidates:
                continue
            callees[caller].append(callee)
            break
    return callees


def get_callers(data):
    callees = get_callees(data)
    callers = {name: [] for name in callees}
    for caller, loc_callees in callees.items():
        for callee in loc_callees:
            callers[callee].append(caller)
    return callers


class Timer:
    """a CallbackDecorator for timing methods

    Notes
    -----
    The time module has several candidates for the clock function:
        - time.perf_counter_ns
        - time.perf_counter (one_second=1)
        - time.monotonic_ns
        - time.monotonic (one_second=1)
        - time.process_time_ns
        - time.process_time (one_second=1)
        - time.time_ns
        - time.time (one_second=1)
    The default is perf_counter_ns as it most suited for profiling.
    Other functions have:
        - less accurate resolution (monotonic*)
        - does not count for external processes (process_time*)
        - are non monotonic (time*)
    """

    clock = time.perf_counter_ns
    one_second = 1e9

    def __init__(self):
        self.data = []

    def __getattr__(self, name):
        def method(*args, **kwds):
            clock = self.clock
            try:
                start = clock()
                yield
                stop = clock()
            except Exception:
                stop = clock()

            tictoc = (name, start, stop)
            self.data.append(tictoc)

        return method

    def get_method_names(self):
        first_start = {}
        for name, start, _ in self.data:
            if start < first_start.get(name, math.inf):
                first_start[name] = start
        return sorted(first_start, key=first_start.get)

    def get_data_at(self, level):
        one = self.one_second
        res = {name: [] for name in self.get_method_names()}
        if level is None:
            for name, start, stop in self.data:
                res[name].append((stop - start) / one)
            return res
        # TODO level

    def get_inner_times(self, level):
        one = self.one_second
        callers = get_callers(self.data)
        data = sorted(self.data, key=lambda v: v[1])
        inner = {name: [] for name in self.get_method_names()}
        for name, start, stop in data:
            duration = (stop - start) / one
            inner[name].append(duration)
            for caller in callers[name]:
                inner[caller][-1] -= duration
        return inner

    def get_inner_cumul(self, level):
        one = self.one_second
        data = self.get_data_at(level)
        callees = get_callees(self.data)
        cumul = {k: sum(v) for k, v in data.items()}
        return {
            k: cumul[k] - sum(cumul[n] for n in callees[k]) for k, v in cumul.items()
        }

    def get_grouped_times(self, data, groups):
        return {name: sum(data.get(n, 0) for n in group) for name, group in groups}

    def report_summary(self, inner=False, level=None):
        inner_str = "inner " if inner else ""
        level_str = "(complete)" if level is None else f"at level *{level}*"
        title = f"== summary of {inner_str}seconds elapsed {level_str} =="
        data = self.get_inner_times(level) if inner else self.get_data_at(level)
        return report_summary(data, title)

    def report_inner_cumul(self, groups=None, level=None):
        index_str = "method" if groups is None else "group"
        level_str = "(complete)" if level is None else f"at level *{level}*"
        title = f"== cumulative inner seconds elapsed by {index_str}s {level_str} =="
        data = self.get_inner_cumul(level)
        if groups is not None:
            data = self.get_grouped_times(data, groups)
        return report_groups(data, title, index_str)
