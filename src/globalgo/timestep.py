import math


class FixedStep:
    """
    fixed time step manager that handle max step,
    the steps can be smaller due to events or to stop exactly at final time
    """

    def __init__(self, step):
        self.step = step
        self.max_step = math.inf

    def __iter__(self):
        return iter([min(self.step, self.max_step)])

    def __str__(self):
        return (
            f"FixedStep with time step {self.step} "
            "(cannot reduce the time step when time step solver fails)"
        )


class DynamicStep:
    __max_step__ = "max_step"
    max_step = None

    def __str__(self):
        return f"DynamicStep (automatic reduced timestep when time step solver fails)"

    def __init__(
        self,
        initial_step,
        maximum_step=None,
        increase_factor=1.2,
        decrease_factor=0.8,
        minimum_step=1e-14,  # CHECKME: machine precision, should not happen when solving
        karma_threshold=None,
        karma_reset=True,
    ):
        assert initial_step > 0
        self.step = initial_step
        self.minimum = minimum_step
        self.maximum = maximum_step
        assert increase_factor > 1
        self.increase_factor = increase_factor
        assert 0 < decrease_factor < 1
        self.decrease_factor = decrease_factor
        self.karma_threshold = karma_threshold or 1
        self.karma_status = self.karma_threshold
        self.karma_reset = karma_reset

    def __iter__(self):
        return self.steps(self.max_step)

    @property
    def step(self):
        return self.current

    @step.setter
    def step(self, value):
        self.previous = None
        self.current = value

    def steps(self, upper_bound=None):
        if self.previous:
            if self.karma_status >= self.karma_threshold:
                self.previous *= self.increase_factor
                if self.karma_reset:
                    self.karma_status = 1
            else:
                self.karma_status += 1
            self.current = self.previous
            if self.maximum is not None:
                self.current = min(self.current, self.maximum)
        self.current = max(self.current, self.minimum)
        # set previous before modifying current
        # then if the visu imposes a small step, the next one will be set
        # using the previous (not so small) step
        self.previous = self.current
        if upper_bound is not None:
            self.current = min(self.current, upper_bound)

        while True:
            # produce at least one value, even if self.step is smaller
            # than self.minimum (possible due to upper_bound)
            yield self.current  # try one time step
            # time step failure : update karma_status, step and previous
            self.karma_status = 1
            self.current *= self.decrease_factor
            self.previous = self.current
            # no convergence before minimum step, stop
            if self.current < self.minimum:
                break
