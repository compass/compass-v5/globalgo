from collections.abc import Callable, Generator
from typing import Protocol, Sequence, Optional, Any

import types
import inspect

from .core import Event, EventPool, Timestepper, Timeloop, Newton

from .core import interfaces as base


__all__ = [
    "TimestepManagerInterface",
    "AssemblerInterface",
    "ResidualNormInterface",
    "LinearSolverInterface",
    "LinearizedProblemInterface",
    "CallbackDecoratorInterface",
]


from typing import TypeVar
from typing_extensions import TypeVarTuple, Unpack

Solution = TypeVar("Solution")
Vector = TypeVar("Vector")
Matrix = TypeVar("Matrix")
Status = TypeVar("Status")
InfoType = TypeVar("InfoType", covariant=True)
InfoTypes = TypeVarTuple("InfoTypes")


class CallbackDecoratorInterface(Protocol):
    "an object that provides decorators for the methods of Timeloop and its components."

    def timeloop_run(
        self, solution, stop, start, events
    ) -> Optional[Generator[None, Any, None]]:
        "decorates timeloop.run()"
        ...

    def timestepper_make_step(
        self, solution, steps, tick
    ) -> Optional[Generator[None, Any, None]]:
        "decorates timestepper.make_step()"
        ...

    def timestepper_try_step(
        self, iteration, solution, dt, tick
    ) -> Optional[Generator[None, Any, None]]:
        "decorates timestepper.try_step()"
        ...

    def newton_solve(
        self, solution, **assembler_options
    ) -> Optional[Generator[None, Any, None]]:
        "decorates newton.solve()"
        ...

    def newton_step(
        self, iteration, solution, dx, status_history
    ) -> Optional[Generator[None, Any, None]]:
        "decorates newton.step()"
        ...

    def assembler_set_up(
        self, solution: Solution, **options: Any
    ) -> Optional[Generator[None, InfoType, None]]:
        "decorates assembler.set_up()"
        ...

    def assembler_evaluate(
        self, solution: Solution
    ) -> Optional[Generator[None, tuple[Vector, Matrix, Unpack[InfoTypes]], None]]:
        "decorates assembler.evaluate()"
        ...

    def assembler_update_solution(
        self, solution: Solution, increment: Vector
    ) -> Optional[Generator[None, Solution, None]]:
        "decorates assembler.update_solution()"
        ...

    def residual_norm_set_up(
        self, solution: Solution, assembler: base.AssemblerInterface
    ) -> Optional[Generator[None, InfoType, None]]:
        "decorates residual_norm.set_up()"
        ...

    def residual_norm_norm(
        self, solution: Solution, residual: Vector, increment: Vector
    ) -> Optional[Generator[None, float | tuple[float, Unpack[InfoTypes]], None]]:
        "decorates residual_norm.norm()"
        ...

    def linear_solver_set_up(
        self, matrix: Matrix
    ) -> Optional[Generator[None, InfoType, None]]:
        "decorates linear_solver.set_up()"
        ...

    def linear_solver_solve(
        self, rhs: Vector
    ) -> Optional[Generator[None, tuple[Vector, Status], None]]:
        "decorates linear_solver.solve()"
        ...


class EventsProviderInterface(Protocol):
    """an object that exposes events to automatically add to the timeloop.

    Attributes
    ----------
    __events__: Optional[Callable[..., Sequence[Event] | EventPool]]
        factory for the events automatically added by the object

    See Also
    --------
    config.configure_timeloop()

    """

    __events__: Optional[Callable[..., Sequence[Event] | EventPool]]


class CallbacksProviderInterface(Protocol):
    """an object that exposes events to automatically add to the timeloop.

    Attributes
    ----------
    __callbacks__: Optional[Callable[..., Sequence[CallbackDecoratorInterface]]]
        factory for the callbacks automatically added by the object

    See Also
    --------
    config.configure_timeloop()

    """

    __callbacks__: Optional[Callable[..., Sequence[CallbackDecoratorInterface]]]


class AssemblerInterface(
    EventsProviderInterface,
    CallbacksProviderInterface,
    base.AssemblerInterface,
    Protocol,
):
    """an object that computes the residual of the non linear problem and its jacobian.
    It is also responsible to update a solution by an increment vector.

    Attributes
    ----------
    __events__: Optional[Callable[..., Sequence[Event] | EventPool]]
        factory for the events automatically added by the object
    __callbacks__: Optional[Callable[..., Sequence[CallbackDecoratorInterface]]]
        factory for the callbacks automatically added by the object

    """

    ...


class ResidualNormInterface(
    EventsProviderInterface,
    CallbacksProviderInterface,
    base.ResidualNormInterface,
    Protocol,
):
    """an object that computes the norm of residual and/or increment vectors.

    Attributes
    ----------
    __events__: Optional[Callable[..., Sequence[Event] | EventPool]]
        factory for the events automatically added by the object
    __callbacks__: Optional[Callable[..., Sequence[CallbackDecoratorInterface]]]
        factory for the callbacks automatically added by the object

    """

    ...


class LinearSolverInterface(
    EventsProviderInterface,
    CallbacksProviderInterface,
    base.LinearSolverInterface,
    Protocol,
):
    """an object that solves linear problems.

    Attributes
    ----------
    __events__: Optional[Callable[..., Sequence[Event] | EventPool]]
        factory for the events automatically added by the object
    __callbacks__: Optional[Callable[..., Sequence[CallbackDecoratorInterface]]]
        factory for the callbacks automatically added by the object

    """

    ...


class TimestepManagerInterface(
    EventsProviderInterface,
    CallbacksProviderInterface,
    base.TimestepManagerInterface,
    Protocol,
):
    """an object that provides the time step strategy.

    It is an iterable object with an "update max_step" mechanism.
    If the "update max_step" mechanism is missing when needed, a warning will be emitted.
    The "update max_step" mechanism can be one of the following:

    - `__update_max_step__(max_step: float)`
      method that get the new max_step
    - `update_max_step(max_step: float)`
      method that get the new max_step
    - `__max_step__: float`
      editable attribute
    - `max_step: float`
      editable attribute

    Attributes
    ----------
    __events__: Optional[Callable[..., Sequence[Event] | EventPool]]
        factory for the events automatically added by the object
    __callbacks__: Optional[Callable[..., Sequence[CallbackDecoratorInterface]]]
        factory for the callbacks automatically added by the object

    """

    ...


class LinearizedProblemInterface(
    EventsProviderInterface,
    CallbacksProviderInterface,
    Protocol,
):
    """an object that exposes the nonlinear problem for one time step.

    Attributes
    ----------
    __assembler__: AssemblerInterface
        computes the residual of the non linear problem and its jacobian.
    __residual_norm__: ResidualNormInterface
        computes the norm of the residual and/or the newton increment.
    __events__: Optional[Callable[..., Sequence[Event] | EventPool]]
        factory for the events automatically added by the object
    __callbacks__: Optional[Callable[..., Sequence[CallbackDecoratorInterface]]]
        factory for the callbacks automatically added by the object

    """

    __assembler__: AssemblerInterface
    __residual_norm__: ResidualNormInterface
