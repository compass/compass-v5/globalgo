import copy
import itertools

from .core import EventPool, Timeloop, Timestepper, Newton, FixedPoint
from .core.utils import F
from .callbacks import with_callbacks
from . import config_namespaces


class ConfigHelper:
    """helper object to build sub dicts used in configure_timeloop

    >>> H = ConfigHelper()
    >>> H.a.b(x=1, y=2)
    ... {'__type__': 'a.b', 'x': 1, 'y': 2}
    >>> H.a.b(x=1, y=H.c(y=2))
    ... {'__type__': 'a.b', 'x': 1, 'y': {'__type__': 'c', 'y': 2}}

    """

    __slots__ = ("__name__", "__type_key__")

    def __init__(self, name="", type_key="__type__"):
        super().__setattr__("__name__", str(name))
        super().__setattr__("__type_key__", str(type_key))

    def __setattr__(self, name, value):
        raise AttributeError(
            f"attribute '{name}' of '{type(self).__name__}' object can not be set"
        )

    def __getattr__(self, name):
        if name.startswith("__"):
            raise AttributeError(
                f"'{type(self).__name__}' object has no attribute '{name}'"
            )
        if self.__name__:
            name = ".".join((self.__name__, name))
        return type(self)(name, self.__type_key__)

    def __call__(self, **kwd):
        base = {self.__type_key__: self.__name__} if self.__name__ else {}
        return {**base, **kwd}

    def __repr__(self):
        return f'{type(self).__name__}(name="{self.__name__}", type_key="{self.__type_key__}")'


class MergeAttributeWrapper:
    def __init__(self, objects):
        self.__dict__["_objects"] = objects

    def __getattr__(self, name):
        attrs = (getattr(obj, name, None) for obj in self._objects)
        return [e for e in attrs if e is not None]

    def __setattr__(self, name, value):
        raise TypeError(f"no attribute setting for {type(self).__qualname__} object")


def resolve(config, namespace=None, type_key="__type__"):
    namespace = namespace or {}
    if isinstance(config, list):
        return [resolve(e, namespace, type_key) for e in config]
    if not isinstance(config, dict):
        return config
    if not type_key in config:
        return {k: resolve(v, namespace, type_key) for k, v in config.items()}
    config = dict(config)
    factory_name = config.pop(type_key)
    try:
        factory = namespace[factory_name]
    except KeyError:
        raise ValueError(
            f"unknown {type_key} value ('{factory_name}'). "
            f"Should be one of {list(namespace)}."
        )
    return factory(**resolve(config, namespace, type_key))


def resolve_with_default(
    config,
    defaults,
    namespace=None,
    type_key="__type__",
):
    defaults = defaults or {}
    namespace = namespace or {}
    namespace = namespace | defaults
    config = list(config or [])
    for name, value in defaults.items():
        if not any(e.get(type_key) == name for e in config):
            config.append({type_key: name})
    return resolve(config, namespace)


def resolve_events(
    config,
    defaults,
    namespace,
    type_key,
):
    objects = resolve_with_default(config, defaults, namespace, type_key)
    assert isinstance(objects, list)
    return EventPool().update(itertools.chain(*objects))


def resolve_callbacks(
    config,
    defaults,
    namespace,
    type_key,
):
    objects = resolve_with_default(config, defaults, namespace, type_key)
    assert isinstance(objects, list)
    return MergeAttributeWrapper(objects)


# TODO remove this (but keep some doc ?)
def configure_timeloop_old(
    linearized_problem,
    config,
    *,
    namespaces=None,
):
    """Builds a fully configured Timeloop object.

     - `linearized_problem` defines the problem to solve
     - `config` defines the solving parameters

    The Timeloop object is composed of several components:

    `events` : `EventPool`
        It is processed after each time loop.
        Passed to the `events` argument of `Timeloop()`.

    `timesteps` : `TimestepManagerInterface`
        It provides the timestep strategy.
        Passed to the `timestep_manager` argument of `Timeloop()`.

    `timestepper` : `Timestepper`
        It handles the timestep attempts.
        Passed to the `timestepper` argument of `Timeloop()`.

    `newton` : `Newton`
        It solves the non linear problem of advancing one timestep.
        Passed to `Timestepper()`.

    `assembler` : `AssemblerInterface`
        It computes the residual of the non linear problem and its jacobian.
        Passed to the `assembler` argument of `Newton()`.

    `residual_norm` : `ResidualNormInterface`
        It computes norm of the residual and/or the newton increment.
        Passed to the `residual_norm` argument of `Newton()`.

    `linear_solver` : `LinearSolverInterface`
        It solves the linear problem provided by `assembler`.
        Passed to the `linear_solver` argument of `Newton()`.

    `callbacks` : `CallbackDecoratorInterface`
        It provides decorators for the methods of Timeloop and its components.

    Parameters
    ----------
    linearized_problem : LinearizedProblemInterface
        Provides the problem definition, especially the assembler (AssemblerInterface)
        and the residual norm (ResidualNormInterface) but also the default events and callbacks.

    config : dict
        Provides the configuration data to build the `Timeloop` object.

        Objects can be passed directly or using the factories defined by `namespaces`.
        The latter case is triggered when a dict value contains the key "__type__".
        The value associated to the key "__type__" is resolved in the corresponding namespace
        to get the factory and the other keys are passed as keyword arguments to the factory.

        Each key of `config` provides a value (dict/list/object) used to define
        a component (see list above) of the `Timeloop` object.
        The possible keys are:
        "newton", "linear_solver", "timesteps", "events", "callbacks".

        `config["newton"]` : dict
            keywords dict passed to `Newton()`.
            It must only define the keys : "tolerance" and "maxiter".

        `config["linear_solver"]` : `LinearSolverInterface`
            used to define the `linear_solver` argument of `Newton()`.

        `config["timesteps"]` : `TimestepManagerInterface`
            used to define the `timestep_manager` argument of `Timeloop()`.

        `config["events"]` : list of sequences of `Event` objects
            All the sequences are merged into one `EventPool` that is passed to
            the `events` argument of `Timeloop()`.
            The `EventPool` object is also augmented with the EventsProviderInterface objects (see Notes).

        `config["callbacks"]` : list of `CallbackDecoratorInterface` objects
            All the callback objects are stacked into one that is used to decorate the methods
            of `Timeloop` and its components.
            The list of callback object is also augmented with the CallbacksProviderInterface objects (see Notes).

    Returns
    -------
    Timeloop
        the fully configured Timeloop object

    See Also
    --------
    ConfigHelper: build "__type__" dict using method call

    core.Timeloop
    core.Newton
    core.EventPool

    interfaces.LinearizedProblemInterface
    interfaces.CallbackDecoratorInterface
    interfaces.TimestepManagerInterface
    interfaces.AssemblerInterface
    interfaces.ResidualNormInterface
    interfaces.LinearSolverInterface

    Other Parameters
    ----------------
    namespaces : dict of dict, optional
        Provides the namespaces used to resolve the factories marked as "__type__" in `config`.

        Default value (=None) means `config_namespaces.get_namespaces()` is used to fill `namespaces`.

        If provided, the dict can define the keys:
        "linear_solver" / "timesteps" / "events" / "callbacks".
        Each key defines a corresponding namespace as a sub dict structured as {name: factory}.

        `namespaces["linear_solver"]`
            contains the factories usable for the `linear_solver` argument of `Newton()`.

        `namespaces["timesteps"]`
            contains the factories usable for the `timestep_manager` argument of `Timeloop()`.

        `namespaces["events"]`
            contains factories usable for building the `EventPool` objects of `config["events"]`.
            This namespaces is augmented by the EventsProviderInterface objects (see Notes).

        `namespaces["callbacks"]`
            contains factories usable for building the `CallbackDecoratorInterface`
            objects of `config["callbacks"]`.
            This namespaces is augmented by the CallbacksProviderInterface objects (see Notes).

    Notes
    -----
    The EventsProviderInterface / CallbacksProviderInterface objects are associated to factory names:
        - `linearized_problem`                   -> "default"
        - `linearized_problem.__assembler__`     -> "assembler"
        - `linearized_problem.__residual_norm__` -> "residual_norm"
        - `config["linear_solver"]`              -> "linear_solver"
        - `config["timesteps"]`                  -> "timesteps"
    The associated str are the automatic names of the factories in the "events"/"callbacks" namespace.
    These names can be used to provide custom parameters to the factories.
    If these names are omitted, the corresponding factories are called with default arguments.

    """

    type_key = "__type__"

    if namespaces is None:
        # local import to reduce dependencies, only loaded if needed
        from . import config_namespaces

        namespaces = config_namespaces.get_namespaces()

    assembler = linearized_problem.__assembler__
    residual_norm = linearized_problem.__residual_norm__
    linear_solver = resolve(
        config.get("linear_solver"), namespaces.get("linear_solver"), type_key
    )
    timesteps = resolve(config.get("timesteps"), namespaces.get("timesteps"), type_key)
    newton_options = resolve(config.get("newton"), None, type_key)

    # TODO: is "default" the best name ?
    providers = {
        "default": linearized_problem,
        "assembler": assembler,
        "residual_norm": residual_norm,
        "linear_solver": linear_solver,
        "timesteps": timesteps,
    }

    default_events = {
        k: x for k, v in providers.items() if (x := getattr(v, "__events__", None))
    }
    events = resolve_events(
        config.get("events"), default_events, namespaces.get("events"), type_key
    )

    default_callbacks = {
        k: x for k, v in providers.items() if (x := getattr(v, "__callbacks__", None))
    }
    cb = resolve_callbacks(
        config.get("callbacks"),
        default_callbacks,
        namespaces.get("callbacks"),
        type_key,
    )

    # copy to avoid that decorations modify them
    assembler = copy.copy(assembler)
    residual_norm = copy.copy(residual_norm)
    linear_solver = copy.copy(linear_solver)

    return with_callbacks(*cb.timeloop_run)("run")(
        Timeloop(
            events=events,
            timestep_manager=timesteps,
            timestepper=F(
                with_callbacks(*cb.timestepper_make_step)("make_step"),
                with_callbacks(*cb.timestepper_try_step)("try_step"),
            )(
                Timestepper(
                    F(
                        with_callbacks(*cb.newton_solve)("solve"),
                        with_callbacks(*cb.newton_step)("step"),
                    )(
                        Newton(
                            assembler=F(
                                with_callbacks(*cb.assembler_evaluate)("evaluate"),
                                with_callbacks(*cb.assembler_set_up)("set_up"),
                                with_callbacks(*cb.assembler_update_solution)(
                                    "update_solution"
                                ),
                            )(assembler),
                            residual_norm=F(
                                with_callbacks(*cb.residual_norm_norm)("norm"),
                                with_callbacks(*cb.residual_norm_set_up)("set_up"),
                            )(residual_norm),
                            linear_solver=F(
                                with_callbacks(*cb.linear_solver_solve)("solve"),
                                with_callbacks(*cb.linear_solver_set_up)("set_up"),
                            )(linear_solver),
                            **newton_options,
                        ),
                    ),
                ),
            ),
        ),
    )


def configure_timeloop(problem_spec, config, *, namespaces=None):
    """
    see doc of problem_spec
    """

    type_key = "__type__"

    if namespaces is None:
        namespaces = config_namespaces.get_namespaces()

    timesteps = resolve(config.get("timesteps"), namespaces.get("timesteps"), type_key)

    init = problem_spec.get_solver_initializer(config, namespaces, type_key)
    providers = {"timesteps": timesteps, **init.providers}

    default_events = {
        k: x for k, v in providers.items() if (x := getattr(v, "__events__", None))
    }
    events = resolve_events(
        config.get("events"), default_events, namespaces.get("events"), type_key
    )

    default_callbacks = {
        k: x for k, v in providers.items() if (x := getattr(v, "__callbacks__", None))
    }
    cb = resolve_callbacks(
        config.get("callbacks"),
        default_callbacks,
        namespaces.get("callbacks"),
        type_key,
    )

    step_solver = init.configure_step_solver(cb)

    return with_callbacks(*cb.timeloop_run)("run")(
        Timeloop(
            events=events,
            timestep_manager=timesteps,
            timestepper=F(
                with_callbacks(*cb.timestepper_make_step)("make_step"),
                with_callbacks(*cb.timestepper_try_step)("try_step"),
            )(
                Timestepper(step_solver),
            ),
        ),
    )


class NewtonInitializer:
    def __init__(self, linearized_problem, config, namespaces, type_key):
        self.assembler = copy.copy(linearized_problem.__assembler__)
        self.residual_norm = copy.copy(linearized_problem.__residual_norm__)
        self.linear_solver = resolve(
            config.get("linear_solver"), namespaces.get("linear_solver"), type_key
        )
        self.newton_options = resolve(config.get("newton"), None, type_key)
        # TODO: is "default" the best name ?
        self.providers = {
            "default": linearized_problem,
            "assembler": self.assembler,
            "residual_norm": self.residual_norm,
            "linear_solver": self.linear_solver,
        }

    def configure_step_solver(self, callbacks):
        cb = callbacks
        return F(
            with_callbacks(*cb.newton_solve)("solve"),
            with_callbacks(*cb.newton_step)("step"),
        )(
            Newton(
                **self.newton_options,
                assembler=F(
                    with_callbacks(*cb.assembler_evaluate)("evaluate"),
                    with_callbacks(*cb.assembler_set_up)("set_up"),
                    with_callbacks(*cb.assembler_update_solution)("update_solution"),
                )(self.assembler),
                residual_norm=F(
                    with_callbacks(*cb.residual_norm_norm)("norm"),
                    with_callbacks(*cb.residual_norm_set_up)("set_up"),
                )(self.residual_norm),
                linear_solver=F(
                    with_callbacks(*cb.linear_solver_solve)("solve"),
                    with_callbacks(*cb.linear_solver_set_up)("set_up"),
                )(self.linear_solver),
            ),
        )


class FixedPointInitializer:
    def __init__(self, fixedpoint_problem, config, namespaces, type_key):
        self.function = copy.copy(fixedpoint_problem.__function__)
        self.residual_norm = copy.copy(fixedpoint_problem.__residual_norm__)
        self.fixedpoint_options = resolve(config.get("fixedpoint"), None, type_key)
        # TODO: is "default" the best name ?
        self.providers = {
            "default": fixedpoint_problem,
            "function": self.function,
            "residual_norm": self.residual_norm,
        }

    def configure_step_solver(self, callbacks):
        cb = callbacks
        return F(
            with_callbacks(*cb.fixedpoint_solve)("solve"),
            with_callbacks(*cb.fixedpoint_step)("step"),
        )(
            FixedPoint(
                **self.fixedpoint_options,
                function=F(
                    with_callbacks(*cb.function_evaluate)("evaluate"),
                    with_callbacks(*cb.function_set_up)("set_up"),
                )(self.function),
                residual_norm=F(
                    with_callbacks(*cb.residual_norm_norm)("norm"),
                    with_callbacks(*cb.residual_norm_set_up)("set_up"),
                )(self.residual_norm),
            ),
        )
