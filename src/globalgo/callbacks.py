from types import FunctionType
import inspect
import functools


class ListLog:
    def __init__(self, with_kwds=True):
        self.data = []
        self.with_kwds = with_kwds

    def __getattr__(self, name):
        return getattr(self.data, name)

    def __iter__(self):
        return iter(self.data)

    def __getitem__(self, idx):
        return self.data[idx]

    def __call__(self, *args, **kwargs):
        if self.with_kwds:
            self.append((args, kwargs))
        else:
            assert not kwargs
            self.append(args)


def _decorate_with_gen(cb, func):
    @functools.wraps(func)
    def wrapped(*args, **kwargs):
        gen = cb(*args, **kwargs)
        assert next(gen) is None, "cb should yield None"
        try:
            res = func(*args, **kwargs)
        except Exception as exc:
            try:
                gen.throw(exc)
            except StopIteration:
                pass
            except Exception as cb_exc:
                if cb_exc is not exc:
                    raise
            raise
        try:
            gen.send(res)
        except StopIteration:
            pass
        except Exception:
            raise
        else:
            assert False, "cb yield at most once"
        return res

    return wrapped


def _callback_decorate(cb, func):
    if inspect.isgeneratorfunction(cb):
        return _decorate_with_gen(cb, func)

    @functools.wraps(func)
    def wrapped(*args, **kwargs):
        cb(*args, **kwargs)
        return func(*args, **kwargs)

    return wrapped


def with_callbacks(*callbacks):
    def method_wrapper(obj, method_name):
        method = getattr(obj, method_name)

        wrapped = method
        for c in reversed(callbacks):
            wrapped = _callback_decorate(c, wrapped)

        if isinstance(obj, FunctionType):
            return wrapped
        setattr(obj, method_name, wrapped)
        return obj

    def wrapper(obj):
        if isinstance(obj, str):
            method_name = obj
            return lambda obj: method_wrapper(obj, method_name)
        return method_wrapper(obj, "__call__")

    return wrapper
