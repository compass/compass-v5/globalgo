from collections.abc import Iterable
from typing import Protocol, TypeVar, Any
from typing_extensions import TypeVarTuple, Unpack


Solution = TypeVar("Solution")
Vector = TypeVar("Vector")
Matrix = TypeVar("Matrix")
Status = TypeVar("Status")
InfoType = TypeVar("InfoType", covariant=True)
InfoTypes = TypeVarTuple("InfoTypes")


class ConvergenceFailure(Exception):
    pass


class TimestepManagerInterface(Iterable[float], Protocol):
    """An iterable object with an "update max_step" mechanism.
    If the "update max_step" mechanism is missing when needed, a warning will be emitted.
    The "update max_step" mechanism can be one of the following:

    `__update_max_step__(max_step: float)`
        method that get the new max_step

    `update_max_step(max_step: float)`
        method that get the new max_step

    `__max_step__: float`
        editable attribute

    `max_step: float`
        editable attribute
    """

    # TODO


class AssemblerInterface(Protocol[InfoType]):
    def set_up(self, solution: Solution, **options: Any) -> InfoType:
        """Called once at the beginning of `Newton.solve(solution, **options)` with the same arguments.
        Info (InfoType) are not used by Newton.
        """

    def evaluate(self, solution: Solution) -> tuple[Vector, Matrix, Unpack[InfoTypes]]:
        """Computes the residual and its jacobian.
        Info (InfoTypes) are not used by Newton.
        """

    def update_solution(self, solution: Solution, increment: Vector) -> Solution:
        """Computes the solution updated by an increment."""


class ResidualNormInterface(Protocol[InfoType]):
    def set_up(self, solution: Solution, assembler: AssemblerInterface) -> InfoType:
        """Called once at the beginning of `Newton.solve(solution, **options)`
        with the same `solution` argument.
        Info (InfoType) are not used by Newton.
        """

    def norm(
        self, solution: Solution, residual: Vector, increment: Vector
    ) -> float | tuple[float, Unpack[InfoTypes]]:
        """Computes the residual norm used for the stopping criteria of `Newton.step()`.
        Info (InfoTypes) are not used by Newton.
        """


class LinearSolverInterface(Protocol[InfoType]):
    def set_up(self, matrix: Matrix) -> InfoType:
        """Passes the matrix of the linear problem.
        Info (InfoType) are not used by Newton.
        """

    def solve(self, rhs: Vector) -> tuple[Vector, Status]:
        """Computes the solution of the linear problem."""
