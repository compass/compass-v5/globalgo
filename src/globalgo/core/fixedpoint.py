from typing import NamedTuple, Any
import numpy.linalg
from .interfaces import ConvergenceFailure
from .utils import as_tuple


class FixedpointConvergenceFailure(ConvergenceFailure):
    pass


class FixedPointIterationStatus(NamedTuple):
    iteration: int
    residual_norm: float
    function_status: Any


class SimpleFixedPointNorm:
    def __init__(self, order=None):
        self.order = order

    def set_up(self, initial_solution, **options):
        pass

    def norm(self, solution, solution_prev):
        y = solution - solution_prev
        return numpy.linalg.norm(y, ord=self.order)


class FixedPoint:
    __exceptions__ = (FixedpointConvergenceFailure,)
    default_residual_norm = SimpleFixedPointNorm

    def __init__(
        self,
        tolerance,
        maxiter,
        function,
        residual_norm=None,
    ):
        self.tolerance = tolerance
        self.maxiter = maxiter
        self.function = function
        self.residual_norm = residual_norm or self.default_residual_norm()

    def solve(self, solution, **func_options):
        self.function.set_up(solution, **func_options)
        self.residual_norm.set_up(solution, **func_options)
        status_history = []
        for iteration in range(1, self.maxiter + 1):
            converged, solution = self.step(iteration, solution, status_history)
            if converged:
                return solution, status_history
        raise FixedpointConvergenceFailure(status_history)

    def step(self, iteration, solution, status_history):
        solution_prev = solution
        solution, func_status = self.function.evaluate(solution)
        residual_norm = self.residual_norm.norm(solution, solution_prev)
        residual_norm = as_tuple(residual_norm)[0]
        status = FixedPointIterationStatus(iteration, residual_norm, func_status)
        status_history.append(status)
        return (residual_norm < self.tolerance), solution
