import warnings
import math
import copy
from dataclasses import dataclass
from typing import NamedTuple
from sortedcontainers import SortedList
import itertools

from typing import Optional
from collections.abc import Callable


class AllAttemptsFailed(Exception):
    def __init__(self, steps, attempt_history):
        self.steps = steps
        self.attempt_history = attempt_history


class TimeTick(NamedTuple):
    time: Optional[float] = None
    iteration: Optional[int] = None

    @classmethod
    def from_time(cls, value, default=None):
        res = value if isinstance(value, cls) else cls(time=value)
        return cls(
            default if res.time is None else res.time,
            default if res.iteration is None else res.iteration,
        )

    @classmethod
    def from_iteration(cls, value, default=None):
        res = value if isinstance(value, cls) else cls(iteration=value)
        return cls(
            default if res.time is None else res.time,
            default if res.iteration is None else res.iteration,
        )


class StepAttemptStatus(NamedTuple):
    iteration: int
    failure: Optional[Exception]
    dt: float
    tick: TimeTick


class ApplyOnce(NamedTuple):
    def __call__(self, pool, action, solution, tick):
        return action(solution, tick)


class ApplyRecurringTime(NamedTuple):
    period: float

    def __call__(self, pool, action, solution, tick):
        pool.add_time(tick.time + self.period, action, self)
        return action(solution, tick)


class ApplyRecurringIteration(NamedTuple):
    period: int

    def __call__(self, pool, action, solution, tick):
        pool.add_iteration(tick.iteration + self.period, action, self)
        return action(solution, tick)


def recurring(time=None, iteration=None):
    if None not in (time, iteration):
        raise ValueError(
            f"'time' (={time}) and 'iteration' (={iteration}) cannot be used together"
        )
    elif time is not None:
        return ApplyRecurringTime(time)
    elif iteration is not None:
        return ApplyRecurringIteration(iteration)
    else:
        raise ValueError("one of 'time' or 'iteration' must be set")


class Event(NamedTuple):
    tick: TimeTick
    action: Callable
    apply: ApplyOnce | ApplyRecurringTime | ApplyRecurringIteration = ApplyOnce()


@dataclass
class EventBox:
    event: Optional[Event] = None


class EventPool:
    def __init__(self):
        self._time_events = SortedList(
            key=lambda b: b.event.tick.time if b.event else -math.inf
        )
        self._iteration_events = SortedList(
            key=lambda b: b.event.tick.iteration if b.event else -math.inf
        )

    def __iter__(self):
        boxes = itertools.chain(self._time_events, self._iteration_events)
        for b in boxes:
            if event := b.event:
                yield event

    def __bool__(self):
        return bool(self._time_events) or bool(self._iteration_events)

    def min_time(self):
        return self._min_seq(self._time_events).tick.time

    def min_iteration(self):
        return self._min_seq(self._iteration_events).tick.iteration

    def _min_seq(self, seq):
        self._clean(seq)
        if not seq:
            return None
        return seq[0].event

    def _clean(self, seq):
        while seq and seq[0].event is None:
            seq.pop(0)

    def process(self, solution, tick):
        def do(seq, upper):
            if upper is None:
                return
            to_remove = []
            for i, box in enumerate(seq):
                if box.event is None:
                    to_remove.append(i)
                    continue
                if seq.key(box) > upper:
                    break
                box.event.apply(self, box.event.action, solution, tick)
                box.event = None
                to_remove.append(i)
            to_remove.reverse()
            for i in to_remove:
                seq.pop(i)

        do(self._time_events, tick.time)
        do(self._iteration_events, tick.iteration)

    def update(self, events):
        for event in events:
            self.add(event)
        return self

    def add(self, event):
        assert isinstance(event, Event)
        box = EventBox(event)
        if event.tick.time is not None:
            self._time_events.add(box)
        if event.tick.iteration is not None:
            self._iteration_events.add(box)
        return self

    def add_time(self, time, action, apply=None):
        self.add(Event(TimeTick(time), action, apply or ApplyOnce()))
        return self

    def add_iteration(self, iteration, action, apply=None):
        self.add(Event(TimeTick(None, iteration), action, apply or ApplyOnce()))
        return self

    def add_recurring_time(self, period, action, start=0):
        assert start + period > start
        self.add_time(start, action, ApplyRecurringTime(period))
        return self

    def add_recurring_iteration(self, period, action, start=0):
        assert start + period > start
        self.add_iteration(start, action, ApplyRecurringIteration(period))
        return self


class Timeloop:
    def __init__(self, timestepper, timestep_manager, events=None, callbacks=()):
        self.timestepper = timestepper
        self.timestep_manager = timestep_manager
        self.events = events or EventPool()
        assert isinstance(self.events, EventPool)
        for action in callbacks:
            self.events.add_recurring_iteration(1, action)

    def run(self, solution, stop, start=None, events=None):
        start = TimeTick.from_time(start, 0)
        stop = TimeTick.from_time(stop, math.inf)
        assert set(stop) != {math.inf}

        run_events = events
        events = EventPool().update(self.events)
        events.add_time(stop.time, lambda *a: None)
        if run_events:
            events.update(run_events)

        tick = start
        events.process(solution, tick)
        while tick.time < stop.time and tick.iteration < stop.iteration:
            self._update_max_step(self.timestep_manager, events.min_time() - tick.time)
            solution, dt, *status = self.timestepper.make_step(
                solution,
                self.timestep_manager,
                tick,
            )
            tick = TimeTick(tick.time + dt, tick.iteration + 1)
            events.process(solution, tick)
        return solution, tick

    @staticmethod
    def _update_max_step(step_manager, step):
        for name in ["__update_max_step__", "update_max_step"]:
            if f := getattr(step_manager, name, None):
                return f(step)
        name = getattr(step_manager, "__max_step__", "max_step")
        if not isinstance(name, str):
            name = "__max_step__"
        try:
            setattr(step_manager, name, step)
            return
        except AttributeError:
            pass
        warnings.warn("unable to update maximum step")


class Timestepper:
    def __init__(self, step_solver):
        self.step_solver = step_solver
        self.solver_exceptions = getattr(step_solver, "__exceptions__", ())

    def make_step(self, solution, steps, tick):
        saved_solution = copy.deepcopy(solution)
        attempt_history = []
        for i, dt in enumerate(steps):
            solution = saved_solution
            solution, status = self.try_step(i, solution, dt, tick)
            attempt_history.append(status)
            if not status.failure:
                return solution, dt, attempt_history
        raise AllAttemptsFailed(steps, attempt_history)

    def try_step(self, iteration, solution, dt, tick):
        exceptions = self.solver_exceptions
        try:
            solution, status = self.step_solver.solve(solution, dt=dt, time=tick.time)
        except exceptions as err:
            return solution, StepAttemptStatus(iteration, err, dt, tick)
        else:
            return solution, StepAttemptStatus(iteration, None, dt, tick)
