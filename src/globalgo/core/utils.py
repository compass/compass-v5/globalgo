from typing import Any, TypeVar
from collections.abc import Callable

T = TypeVar("T")


def as_tuple(obj: tuple | T) -> tuple | tuple[T]:
    "returns a tuple: either obj or (obj,)"
    if isinstance(obj, tuple):
        return obj
    return (obj,)


def chain_func(func: Callable, *funcs) -> Callable:
    """
    chain_func('-'.join, sorted, str.split)("a bc ab ba") -> 'a-ab-ba-bc'
    """
    funcs = tuple(reversed([func, *funcs]))

    def chained(*args):
        for func in funcs:
            args = func(*as_tuple(args))
        return args

    return chained


F = chain_func


f_true = lambda *args: True
f_false = lambda *args: False
compose = lambda reducer: lambda *funcs: lambda *args: reducer(f(*args) for f in funcs)
f_all = compose(all)
f_any = compose(any)
