from typing import NamedTuple, Optional
import numpy.linalg

from compass_utils.exceptions import LinearSolverFailure

from .interfaces import (
    AssemblerInterface,
    ResidualNormInterface,
    LinearSolverInterface,
    ConvergenceFailure,
)

from .utils import as_tuple, f_false


class NewtonConvergenceFailure(ConvergenceFailure):
    REASONS = ("maxiter", "give_up")

    def __init__(self, reason, history):
        assert reason in self.REASONS
        self.reason = reason
        self.history = history

    def __str__(self):
        return f"{type(self).__name__}(reason={self.reason}, history={self.history})"


class NewtonIterationStatus(NamedTuple):
    iteration: int
    residual_norm: float
    ls_status: list


class SimpleNewtonNorm:
    def __init__(self, type="residual", order=None):
        assert type in ("residual", "increment")
        self.type = type
        self.order = order

    def set_up(self, initial_solution, assembler):
        pass

    def norm(self, solution, residual, dx):
        y = {"residual": residual, "increment": dx}[self.type]
        if y is None:
            return None
        return numpy.linalg.norm(y, ord=self.order)


class Newton:
    __exceptions__ = (NewtonConvergenceFailure, LinearSolverFailure)
    default_residual_norm = SimpleNewtonNorm

    def __init__(
        self,
        tolerance,
        maxiter,
        assembler: AssemblerInterface,
        linear_solver: LinearSolverInterface,
        residual_norm: Optional[ResidualNormInterface] = None,
        give_up=f_false,
        adaptive_linear_solver_tolerance=None,
    ):
        self.linear_solver = linear_solver
        self.assembler = assembler
        self.tolerance = tolerance
        self.maxiter = maxiter
        self.residual_norm = residual_norm or self.default_residual_norm()
        self._give_up = give_up
        self.adaptive_linear_solver_tolerance = adaptive_linear_solver_tolerance
        self._lintol = None

    def solve(self, solution, **assembler_options):
        self.assembler.set_up(solution, **assembler_options)
        self.residual_norm.set_up(solution, self.assembler)
        status_history = []
        dx = None
        for iteration in range(1, self.maxiter + 1):
            converged, solution, dx = self.step(iteration, solution, dx, status_history)
            if converged:
                return solution, status_history
            if self.give_up(iteration, solution, dx, status_history):
                failure_reason = "give_up"
                break
        else:
            failure_reason = "maxiter"
        raise NewtonConvergenceFailure(failure_reason, status_history)

    def step(self, iteration, solution, dx, status_history):
        residual, jacobian, *_ = self.assembler.evaluate(solution)
        residual_norm = self.residual_norm.norm(solution, residual, dx)
        residual_norm = as_tuple(residual_norm)[0]
        if (residual_norm is not None) and (residual_norm < self.tolerance):
            status = NewtonIterationStatus(iteration, residual_norm, None)
            status_history.append(status)
            return True, solution, dx
        #
        self.linear_solver.set_up(jacobian)
        lin_tol = self.linear_solver_tolerance(iteration, solution, dx, status_history)
        if lin_tol is None:
            dx, ls_status = self.linear_solver.solve(residual)
        else:
            dx, ls_status = self.linear_solver.solve(residual, tolerance=lin_tol)
        #
        status = NewtonIterationStatus(iteration, residual_norm, ls_status)
        status_history.append(status)
        solution = self.assembler.update_solution(solution, dx)
        return False, solution, dx

    def linear_solver_tolerance(self, iteration, solution, dx, status_history):
        func = self.adaptive_linear_solver_tolerance
        if func is None:
            return None
        try:
            return func(iteration, solution, dx, [*status_history])
        except Exception as exc:
            raise RuntimeError(
                "Unable to evaluate the 'adaptive_linear_solver_tolerance' function."
            ) from exc

    def give_up(self, iteration, solution, dx, status_history):
        try:
            return self._give_up(iteration, solution, dx, status_history)
        except Exception as exc:
            raise RuntimeError("Unable to evaluate the 'give up' condition.") from exc
