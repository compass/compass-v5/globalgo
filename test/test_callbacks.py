import pytest

from globalgo.callbacks import with_callbacks
from globalgo.core.utils import F


def cb_pre(name, log):
    def cb(data):
        log(name, data)

    return cb


def cb_post(name, log):
    def cb(data):
        result = yield
        log(name, result)

    return cb


def cb_pre_post(name, log):
    def cb(data):
        log(name, data)
        result = yield
        log(name, result)

    return cb


def cb_failure(name, log):
    def cb(data):
        log(name, data)
        try:
            result = yield
        except Exception as exc:
            log(name, exc)
        else:
            log(name, result)

    return cb


logger = lambda log: lambda *args: log.append(args)


class FuncException(Exception):
    def __eq__(self, other):
        return (type(self) == type(other)) and (self.args == other.args)


def simple_func(data):
    if data == "fail":
        raise FuncException("simple_func has failed")
    return 42


class SimpleObject:
    def method_a(self, data):
        if data == "fail":
            raise FuncException("method_a has failed")
        return "a"

    def method_b(self, data):
        if data == "fail":
            raise FuncException("method_b has failed")
        return "b"


def test_func_pre():
    log = []
    func = with_callbacks(cb_pre("pre", logger(log)))(simple_func)
    #
    log.clear()
    func("coucou")
    assert log == [("pre", "coucou")]
    #
    log.clear()
    with pytest.raises(FuncException):
        func("fail")
    assert log == [("pre", "fail")]


def test_func_post():
    log = []
    func = with_callbacks(cb_post("post", logger(log)))(simple_func)
    #
    log.clear()
    func("coucou")
    assert log == [("post", 42)]
    #
    log.clear()
    with pytest.raises(FuncException):
        func("fail")
    assert log == []


def test_func_pre_post():
    log = []
    func = with_callbacks(cb_pre_post("pre_post", logger(log)))(simple_func)
    #
    log.clear()
    func("coucou")
    assert log == [("pre_post", "coucou"), ("pre_post", 42)]
    #
    log.clear()
    with pytest.raises(FuncException):
        func("fail")
    assert log == [("pre_post", "fail")]


def test_func_combine_pre_and_post():
    log = []
    func = with_callbacks(cb_pre("pre", logger(log)), cb_post("post", logger(log)))(
        simple_func
    )
    #
    log.clear()
    func("coucou")
    assert log == [("pre", "coucou"), ("post", 42)]
    #
    log.clear()
    with pytest.raises(FuncException):
        func("fail")
    assert log == [("pre", "fail")]


def test_func_failure():
    log = []
    func = with_callbacks(cb_failure("failure", logger(log)))(simple_func)
    #
    log.clear()
    func("coucou")
    assert log == [("failure", "coucou"), ("failure", 42)]
    #
    log.clear()
    with pytest.raises(FuncException):
        func("fail")
    assert log == [
        ("failure", "fail"),
        ("failure", FuncException("simple_func has failed")),
    ]


def test_obj_failure():
    log = []
    obj = F(
        with_callbacks(cb_failure("failure method_a", logger(log)))("method_a"),
        with_callbacks(cb_failure("failure method_b", logger(log)))("method_b"),
    )(SimpleObject())
    #
    log.clear()
    obj.method_a("coucou")
    assert log == [("failure method_a", "coucou"), ("failure method_a", "a")]
    #
    log.clear()
    obj.method_b("coucou")
    assert log == [("failure method_b", "coucou"), ("failure method_b", "b")]
    #
    log.clear()
    with pytest.raises(FuncException):
        obj.method_a("fail")
    assert log == [
        ("failure method_a", "fail"),
        ("failure method_a", FuncException("method_a has failed")),
    ]
    #
    log.clear()
    with pytest.raises(FuncException):
        obj.method_b("fail")
    assert log == [
        ("failure method_b", "fail"),
        ("failure method_b", FuncException("method_b has failed")),
    ]


def test_func_order():
    log = []
    func = with_callbacks(cb_pre_post("A", logger(log)), cb_pre_post("B", logger(log)))(
        simple_func
    )
    #
    log.clear()
    func("coucou")
    assert log == [("A", "coucou"), ("B", "coucou"), ("B", 42), ("A", 42)]
