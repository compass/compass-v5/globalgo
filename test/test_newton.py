import pytest

import numpy as np
from globalgo.core import Newton, NewtonConvergenceFailure
from globalgo.callbacks import with_callbacks


class MyAssembler:
    """assembler for a very simple problem
    F(x) = a * x**2 + b * x + c
    """

    def __init__(self, a, b, c):
        self.a = a
        self.b = b
        self.c = c

    def ls_description(self):
        return None

    def set_up(self, solution):
        pass

    def evaluate(self, solution):
        x = solution
        value = self.a * x**2 + self.b * x + self.c
        gradient = 2 * self.a * x + self.b
        return value, gradient

    def update_solution(self, solution, dx):
        return solution - dx


class ScalarLinearSolver:
    """solver for very simple linear problem
    find x solving A * x = B where A and B are scalars
    """

    def __init__(self, ls_description=None):
        assert ls_description is None

    def set_up(self, A):
        self.A = A

    def solve(self, B):
        status = 0
        return B / self.A, status


def test_simple():
    assembler = MyAssembler(1, 0, -1)
    lin_solver = ScalarLinearSolver()
    nl_solver = Newton(1e-6, 10, assembler, lin_solver)
    solution, status = nl_solver.solve(10)
    assert np.allclose(solution, 1)
    solution, status = nl_solver.solve(-10)
    assert np.allclose(solution, -1)


def test_give_up():
    at = lambda stop: lambda i, x, dx, hist: i >= stop
    assembler = MyAssembler(1, 0, -1)
    lin_solver = ScalarLinearSolver()
    #
    # normal solve take `nit` steps to succeed
    nl_solver = Newton(1e-6, 10, assembler, lin_solver)
    solution, status = nl_solver.solve(10)
    assert np.allclose(solution, 1)
    nit = len(status)
    #
    # give up after `nit` steps is a success, solution found just before giving up
    nl_solver = Newton(1e-6, 10, assembler, lin_solver, give_up=at(nit))
    solution, status = nl_solver.solve(10)
    assert np.allclose(solution, 1)
    #
    # give up after `nit-1` steps fails
    nl_solver = Newton(1e-6, 10, assembler, lin_solver, give_up=at(nit - 1))
    with pytest.raises(NewtonConvergenceFailure):
        nl_solver.solve(10)


def test_adaptive_tolerance():
    """Adapt the linear solver tolerance with the newton iteration"""

    class IterativeLinearSolver:
        def __init__(self, tolerance=1e-8):
            self.tolerance = tolerance

        def set_up(self, A):
            self.A = A

        def solve(self, B, tolerance=None):
            if tolerance is None:
                tolerance = self.tolerance
            status = 0
            # emulate inexact solver
            return (B + tolerance) / self.A, status

    def adapt_1(iteration, solution, dx, status_history):
        if not status_history:
            return 1e-3
        lin_tol = status_history[-1].residual_norm * 1e-2
        return lin_tol

    nl_solver = Newton(
        assembler=MyAssembler(1, 0, -1),
        linear_solver=IterativeLinearSolver(1e-8),
        tolerance=1e-8,
        maxiter=20,
        adaptive_linear_solver_tolerance=adapt_1,
    )
    result, status = nl_solver.solve(10)
    assert len(status) > 8

    def adapt_2(iteration, solution, dx, status_history):
        return 10**-iteration

    nl_solver = Newton(
        assembler=MyAssembler(1, 0, -1),
        linear_solver=IterativeLinearSolver(1e-8),
        tolerance=1e-8,
        maxiter=20,
        adaptive_linear_solver_tolerance=adapt_2,
    )
    result, status = nl_solver.solve(10)
    assert len(status) > 8
