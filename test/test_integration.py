import math

from globalgo.core import Newton, SimpleNewtonNorm, Timeloop, Timestepper, EventPool
from globalgo.timestep import DynamicStep, FixedStep
from globalgo.callbacks import ListLog, with_callbacks
from globalgo.core.utils import F

from test_timeloop import ExpAssembler
from test_newton import ScalarLinearSolver


def after(func):
    def wrapper(*args, **kwargs):
        yield
        func(*args, **kwargs)

    return wrapper


def test_link_everything():
    initial_solution = 1
    tau = 1
    final_time = 5
    dt = 1e-3

    reference = lambda t: initial_solution * math.exp(-tau * t)

    cb_log = ListLog()
    newton_log = ListLog()
    time_log = ListLog()
    Timeloop(
        with_callbacks(cb_log)("make_step")(
            Timestepper(
                F(
                    with_callbacks(cb_log)("solve"),
                    with_callbacks(after(newton_log))("step"),
                )(
                    Newton(
                        assembler=F(
                            with_callbacks(cb_log)("evaluate"),
                            with_callbacks(cb_log)("set_up"),
                            with_callbacks(cb_log)("update_solution"),
                        )(ExpAssembler(tau)),
                        residual_norm=F(
                            with_callbacks(cb_log)("norm"),
                            with_callbacks(cb_log)("set_up"),
                        )(SimpleNewtonNorm()),
                        linear_solver=F(
                            with_callbacks(cb_log)("solve"),
                            with_callbacks(cb_log)("set_up"),
                        )(ScalarLinearSolver()),
                        tolerance=1e-8,
                        maxiter=10,
                    ),
                ),
            ),
        ),
        # DynamicStep(dt),
        FixedStep(dt),
    ).run(
        solution=initial_solution,
        stop=final_time,
        events=EventPool().add_recurring_time(
            period=0.1,
            action=time_log,
        ),
    )

    assert time_log
    assert all(abs(v - reference(t.time)) < dt * 2 for (v, t), _ in time_log)


#     print({m.__self__.__class__.__name__ for (o, m, a, k), _ in cb_log})
#     for o, m, a, k in cb_log[:15]:
#         print(
#             f"-- {m.__self__.__class__.__name__}.{m.__name__}(",
#             *a,
#             f", **{k} )",
#             "\n",
#             o,
#         )
#     # assert 0
