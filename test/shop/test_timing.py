import math
import time

from globalgo.config import ConfigHelper, configure_timeloop, NewtonInitializer
from globalgo.core import EventPool, SimpleNewtonNorm
from globalgo.timestep import DynamicStep
from globalgo.callbacks import ListLog
from globalgo.shop.timing import Timer


class MyAssembler:
    """assembler for a very simple problem
    F(x) = a * x**2 + b * x + c
    """

    def __init__(self, a, b, c):
        self.a = a
        self.b = b
        self.c = c

    def ls_description(self):
        return None

    def set_up(self, solution):
        pass

    def evaluate(self, solution):
        x = solution
        value = self.a * x**2 + self.b * x + self.c
        gradient = 2 * self.a * x + self.b
        return value, gradient

    def update_solution(self, solution, dx):
        return solution - dx


class ExpAssembler(MyAssembler):
    def __init__(self, tau):
        self.tau = tau

    def set_up(self, solution, dt, time):
        self.dt = dt
        self.time = time
        self.prev_solution = solution

    def evaluate(self, solution):
        time.sleep(4e-4)
        c = 1 + self.tau * self.dt
        return c * solution - self.prev_solution, c


class MyProblem:
    __assembler__ = ExpAssembler
    __residual_norm__ = SimpleNewtonNorm

    def __init__(self, tau):
        self.log = ListLog(False)
        self.__assembler__ = self.__assembler__(tau)
        self.__residual_norm__ = self.__residual_norm__()

    def __events__(self, period=math.inf):
        return EventPool().add_recurring_time(period, self.log)

    class __callbacks__:
        def __init__(self, log=print):
            self.log = log

        def timestepper_make_step(self, solution, steps, tick):
            new_solution, step, statuses = yield
            self.log(
                "timestepper_make_step", solution, tick, new_solution, step, statuses
            )

        def assembler_evaluate(self, solution):
            residual, jacobian, *_ = yield
            self.log("assembler_evaluate", solution, residual, jacobian)

    def get_solver_initializer(self, config, namespaces, type_key):
        return NewtonInitializer(self, config, namespaces, type_key)


class ScalarLinearSolver:
    """solver for very simple linear problem
    find x solving A * x = B where A and B are scalars
    """

    def __init__(self, ls_description=None):
        assert ls_description is None

    def set_up(self, A):
        self.A = A

    def solve(self, B):
        time.sleep(4e-3)
        status = 0
        return B / self.A, status


GROUPS = (
    ("linear solver", ("linear_solver_set_up", "linear_solver_solve")),
    (
        "assembling",
        ("assembler_set_up", "assembler_evaluate", "assembler_update_solution"),
    ),
    ("residual norm", ("residual_norm_set_up", "residual_norm_norm")),
    ("newton (remains)", ("newton_solve", "newton_step")),
    ("timestepper (remains)", ("timestepper_make_step", "timestepper_try_step")),
    ("timeloop (remains)", ("timeloop_run",)),
)


def test():
    tau = 1
    u0 = 1
    u = lambda t: u0 * math.exp(-tau * t)
    close = lambda u, v: abs(u - v) < 1e-3
    H = ConfigHelper()
    log = ListLog(False)
    pb = MyProblem(tau)

    timer = Timer()

    config = dict(
        newton=dict(tolerance=1e-30, maxiter=2),
        linear_solver=ScalarLinearSolver(),
        timesteps=DynamicStep(0.1),
        events=[H.default(period=1)],
        callbacks=[
            H.default(log=log),
            timer,
        ],
    )
    loop = configure_timeloop(pb, config)
    solution, tick = loop.run(solution=u0, stop=5)

    print()
    print(timer.report_summary())
    print(timer.report_summary(inner=True))
    # print(timer.report_summary(level="newton_step"))
    print(timer.report_inner_cumul())
    print(timer.report_inner_cumul(GROUPS))
    # print(timer.report_inner_cumul(level="newton_step"))
