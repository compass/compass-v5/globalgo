import pytest

import math

from globalgo.config import (
    resolve,
    ConfigHelper,
    MergeAttributeWrapper,
    resolve_with_default,
    configure_timeloop,
    NewtonInitializer,
)

from globalgo.core import EventPool, SimpleNewtonNorm, TimeTick
from globalgo.timestep import DynamicStep, FixedStep
from globalgo.callbacks import ListLog

from test_timeloop import ExpAssembler
from test_newton import ScalarLinearSolver


def test_resolve():
    assert resolve(42, {}) == 42
    assert resolve({"a": 1, "b": 2}, {}) == {"a": 1, "b": 2}
    assert resolve({"__type__": "T"}, {"T": lambda: 42}) == 42
    assert resolve({"__type__": "T", "a": 1, "b": 2}, {"T": lambda a, b: [a, b]}) == [
        1,
        2,
    ]
    assert resolve({"__type__": "T", "a": 1}, {"T": lambda a, b=-2: [a, b]}) == [1, -2]
    assert resolve({"__type__": "T"}, {"T": lambda a=-1, b=-2: [a, b]}) == [-1, -2]
    assert resolve({"__type__": "T", "b": 2}, {"T": lambda a=-1, b=-2: [a, b]}) == [
        -1,
        2,
    ]
    assert resolve({"x": {"__type__": "T"}}, {"T": lambda: 42}) == {"x": 42}
    assert resolve(
        {"x": {"__type__": "V", "v": {"__type__": "T"}}},
        {"T": lambda: 42, "V": lambda v: [v]},
    ) == {"x": [42]}
    with pytest.raises(ValueError):
        resolve({"x": {"__type__": "T"}}, {"U": lambda: 1, "V": lambda: 2})
    assert resolve([1, 2], {}) == [1, 2]
    assert resolve(
        [{"__type__": "U"}, {"__type__": "V"}], {"U": lambda: 1, "V": lambda: 2}
    ) == [1, 2]


def test_ConfigHelper():
    H = ConfigHelper()
    assert H(x=1, y=2) == {"x": 1, "y": 2}
    assert H.a(x=1, y=2) == {"__type__": "a", "x": 1, "y": 2}
    assert H.a.b(x=1, y=2) == {"__type__": "a.b", "x": 1, "y": 2}
    assert H.a.b(x=1, y=H.c(y=2)) == {
        "__type__": "a.b",
        "x": 1,
        "y": {"__type__": "c", "y": 2},
    }


def test_MergeAttributeWrapper():
    class C:
        def __init__(self, **kwds):
            self.__dict__.update(kwds)

    a = C(x="ax", z="az")
    b = C(y="by", z="bz")
    m = MergeAttributeWrapper([a, b])
    assert m.x == ["ax"]
    assert m.y == ["by"]
    assert m.z == ["az", "bz"]
    assert m.nope == []

    mm = MergeAttributeWrapper([])
    assert mm.x == []
    assert mm.y == []


def test_resolve_with_default():
    # TODO
    pass


class MyProblem:
    __assembler__ = ExpAssembler
    __residual_norm__ = SimpleNewtonNorm

    def __init__(self, tau):
        self.log = ListLog(False)
        self.__assembler__ = self.__assembler__(tau)
        self.__residual_norm__ = self.__residual_norm__()

    def __events__(self, period=math.inf):
        return EventPool().add_recurring_time(period, self.log)

    class __callbacks__:
        def __init__(self, log=print):
            self.log = log

        def timestepper_make_step(self, solution, steps, tick):
            new_solution, step, statuses = yield
            self.log(
                "timestepper_make_step", solution, tick, new_solution, step, statuses
            )

        def assembler_evaluate(self, solution):
            residual, jacobian, *_ = yield
            self.log("assembler_evaluate", solution, residual, jacobian)

    def get_solver_initializer(self, config, namespaces, type_key):
        return NewtonInitializer(self, config, namespaces, type_key)


def configure_fixedstep_timeloop(H, log, close, pb, u, u0):
    config = dict(
        newton=dict(tolerance=1e-8, maxiter=10),
        linear_solver=ScalarLinearSolver(),
        timesteps=FixedStep(1e-3),
        events=[H.default(period=1)],
        callbacks=[H.default(log=log)],
    )

    loop = configure_timeloop(pb, config)
    solution, tick = loop.run(solution=u0, stop=5)
    log_save = list(log)
    assert close(solution, u(tick.time))
    assert [t.time for v, t in pb.log] == [0, 1, 2, 3, 4, 5]
    assert all(close(v, u(t.time)) for v, t in pb.log)
    l = [t.iteration for n, _, t, *_ in log if n == "timestepper_make_step"]
    assert len(l) >= 5 / 1e-3
    assert l == list(range(max(l) + 1))

    # newton can be reached and its parameters are editable
    assert loop.timestepper.step_solver.maxiter == 10
    assert loop.timestepper.step_solver.tolerance == 1e-8

    log.clear()
    pb.log.clear()
    loop_bis = configure_timeloop(pb, config)
    loop_bis.run(solution=u0, stop=5)
    assert log_save == log.data


def configure_dynamicstep_timeloop(H, log, close, pb, u0):
    config = dict(
        newton=dict(tolerance=1e-8, maxiter=10),
        linear_solver=ScalarLinearSolver(),
        timesteps=DynamicStep(0.01),
        events=[H.default(period=1)],
        callbacks=[H.default(log=log)],
    )
    loop = configure_timeloop(pb, config)
    other_dt = 0.1
    loop.timestep_manager.step = other_dt
    solution, tick = loop.run(solution=u0, stop=TimeTick(0.5, 1))
    assert close(tick.time, other_dt)


def call_multiple_timeloop(H, log, pb, u0):
    config = dict(
        newton=dict(tolerance=1e-8, maxiter=10),
        linear_solver=ScalarLinearSolver(),
        timesteps=DynamicStep(0.1),
        events=[H.default(period=1)],
        callbacks=[H.default(log=log)],
    )
    loop = configure_timeloop(pb, config)
    solution, tick = loop.run(solution=u0, stop=TimeTick(0.5, None))

    # change the timestep manager
    loop.timestep_manager = FixedStep(1)
    solution, tick = loop.run(solution=solution, stop=TimeTick(50.5, None))
    assert tick.iteration == 51


def test_configure_timeloop():
    tau = 1
    u0 = 1
    u = lambda t: u0 * math.exp(-tau * t)
    close = lambda u, v: abs(u - v) < 1e-3
    H = ConfigHelper()
    log = ListLog(False)
    pb = MyProblem(tau)

    configure_fixedstep_timeloop(H, log, close, pb, u, u0)
    configure_dynamicstep_timeloop(H, log, close, pb, u0)
    call_multiple_timeloop(H, log, pb, u0)


class LogTimeStepFailure:
    """LogTimeStepFailure(log, residual_transformer)

    log: function
        called when timestepper.try_step() fails
        arguments: a dict
            { 'tick': tick,
              'dt': dt,
              'trace': list of results from residual_transformer(),
            }
    residual_transformer: function
        called after residual_norm.norm(solution, residual, increment)
        arguments: solution, residual, increment, norm

    """

    def __init__(self, log, residual_transformer):
        self.log = log
        self.residual_transformer = residual_transformer
        self.step_solver_trace = []

    def timestepper_try_step(self, iteration, solution, dt, tick):
        self.step_solver_trace = []
        solution, status = yield
        if status.failure:
            self.log(dict(tick=tick, dt=dt, trace=self.step_solver_trace))
        self.step_solver_trace = []

    def residual_norm_norm(self, solution, residual, increment):
        norm = yield
        self.step_solver_trace.append(
            self.residual_transformer(solution, residual, increment, norm)
        )


def test_inject_additionnal_callbacks_to_configure_timeloop():
    tau = 1
    u0 = 1
    u = lambda t: u0 * math.exp(-tau * t)
    close = lambda u, v: abs(u - v) < 1e-3
    H = ConfigHelper()
    log = ListLog(False)
    newton_failure_log = []
    pb = MyProblem(tau)

    config = dict(
        newton=dict(tolerance=1e-30, maxiter=2),
        linear_solver=ScalarLinearSolver(),
        timesteps=DynamicStep(0.1),
        events=[H.default(period=1)],
        callbacks=[
            H.default(log=log),
            # HERE we inject the additional callback
            LogTimeStepFailure(
                newton_failure_log.append, lambda s, r, i, n: (s, r, i, n)
            ),
        ],
    )
    loop = configure_timeloop(pb, config)
    solution, tick = loop.run(solution=u0, stop=5)

    assert newton_failure_log

    print(newton_failure_log)
