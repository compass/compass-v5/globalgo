from globalgo.core import EventPool, Event, TimeTick, Timeloop
from globalgo.timestep import FixedStep


def test_iter():
    func = lambda *a: 42
    e = EventPool()
    e.add_iteration(1, func)
    e.add_iteration(2, func)
    e.add_iteration(3, func)
    e.add_time(0.1, func)
    e.add_time(0.2, func)
    e.add_time(0.3, func)
    assert set(e) == {
        Event(TimeTick(iteration=1), func),
        Event(TimeTick(iteration=2), func),
        Event(TimeTick(iteration=3), func),
        Event(TimeTick(time=0.1), func),
        Event(TimeTick(time=0.2), func),
        Event(TimeTick(time=0.3), func),
    }

    ee = EventPool()
    ee.add_iteration(0, func)
    assert set(ee) == {
        Event(TimeTick(iteration=0), func),
    }
    ee.update(e)
    assert set(ee) == {
        Event(TimeTick(iteration=0), func),
        Event(TimeTick(iteration=1), func),
        Event(TimeTick(iteration=2), func),
        Event(TimeTick(iteration=3), func),
        Event(TimeTick(time=0.1), func),
        Event(TimeTick(time=0.2), func),
        Event(TimeTick(time=0.3), func),
    }


def test_recurring_iteration():
    log = set()
    e = EventPool()
    e.add_recurring_iteration(3, lambda u, t: log.add(("ooo", t.iteration)))
    e.add_recurring_iteration(5, lambda u, t: log.add(("+++", t.iteration)))
    for i in range(30):
        e.process(None, TimeTick(None, i))

    assert log == {
        ("ooo", 0),
        ("+++", 0),
        ("ooo", 3),
        ("+++", 5),
        ("ooo", 6),
        ("ooo", 9),
        ("+++", 10),
        ("ooo", 12),
        ("+++", 15),
        ("ooo", 15),
        ("ooo", 18),
        ("+++", 20),
        ("ooo", 21),
        ("ooo", 24),
        ("+++", 25),
        ("ooo", 27),
    }


class ListLog(list):
    def __call__(self, *obj):
        self.append(obj)


class ExplicitStep:
    def __init__(self, func, status="all good !"):
        self.func = func
        self.status = status

    def make_step(self, solution, steps, tick):
        for dt in steps:
            solution += dt * self.func(solution, tick.time)
            return solution, dt, self.status


def test_event_in_timeloop():
    u0 = 1
    tf = 1
    dt = 1e-3

    F = lambda u, t: 1
    log = ListLog()
    log_event = ListLog()
    tl = Timeloop(ExplicitStep(F), FixedStep(dt), callbacks=[log])
    tl.run(u0, tf, events=EventPool().add_recurring_time(0.1, log_event))
    u, t = log[-1]
    assert t.time == tf
    assert len(log) == t.iteration + 1
    assert abs(u - u0 - t.time) < 1e-10
    assert len(log_event) == tf / 0.1 + 1
    for i, (u, t) in enumerate(log_event):
        assert abs(t.time - i * 0.1) < 1e-10


def test_event_in_timeloop_forced_times():
    u0 = 1
    tf = 1
    dt = 0.1

    F = lambda u, t: 1
    log = ListLog()
    log_event = ListLog()
    tl = Timeloop(ExplicitStep(F), FixedStep(dt), callbacks=[log])
    tl.run(
        u0,
        tf,
        events=EventPool()
        .add_time(0.13, log_event)
        .add_time(0.42, log_event)
        .add_time(0.69, log_event),
    )
    u, t = log[-1]
    assert abs(t.time - tf) < 1e-10
    assert len(log) == t.iteration + 1
    assert abs(u - u0 - t.time) < 1e-10
    assert len(log_event) == 3
    assert abs(log_event[0][1].time - 0.13) < 1e-8
    assert abs(log_event[1][1].time - 0.42) < 1e-8
    assert abs(log_event[2][1].time - 0.69) < 1e-8
