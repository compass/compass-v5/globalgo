import math
from collections import UserList

from globalgo.core import Timeloop, Timestepper, TimeTick, Newton
from globalgo.timestep import DynamicStep
from test_newton import MyAssembler, ScalarLinearSolver


class ListLog(list):
    def __call__(self, *obj):
        self.append(obj)


# The polynomial that newton has to solve has time dependant coefficients.
# The roots of the polynomial translate linearly in time along the x axis
# when offset = 0
# Setting the offset parameter to a high value will cause the
# polynomial to have no roots
# and cause a convergence failure of the Newton algorithm
def time_variadic_evaluate(asb, solution, dt, time):
    x = solution
    t = time
    v = asb.a
    k = asb.b
    offset = dt
    value = (x - v * t) * (x - (v * t + k)) + offset
    gradient = 2 * x - 2 * v * t - k
    return value, gradient


class MyTimeAssembler(MyAssembler):
    def set_up(self, solution, dt, time):
        self.dt = dt
        self.time = time

    def evaluate(self, solution):
        return time_variadic_evaluate(self, solution, self.dt, self.time)


def print_tick(solution, tick):
    print(tick)


def test_timeloop():
    start_dt = 0.1
    u0 = -4.0
    assembler = MyTimeAssembler(1.0, 1.0, 0.0)
    lin_solver = ScalarLinearSolver()
    newton = Newton(1e-6, 20, assembler, lin_solver)
    timestepper = Timestepper(newton)
    log = ListLog()
    timeloop = Timeloop(
        timestepper=timestepper,
        timestep_manager=DynamicStep(start_dt),
        callbacks=[print_tick, log],
    )
    timeloop.run(u0, TimeTick(iteration=10))
    print(log)


class ExplicitStep:
    def __init__(self, func, status="all good !"):
        self.func = func
        self.status = status

    def make_step(self, solution, steps, tick):
        for dt in steps:
            solution += dt * self.func(solution, tick.time)
            return solution, dt, self.status


class ExpAssembler(MyAssembler):
    def __init__(self, tau):
        self.tau = tau

    def set_up(self, solution, dt, time):
        self.dt = dt
        self.time = time
        self.prev_solution = solution

    def evaluate(self, solution):
        c = 1 + self.tau * self.dt
        return c * solution - self.prev_solution, c


unpack_args = lambda f: lambda a: f(*a)
pack_args = lambda f: lambda *a: f(a)


def test_solve_exponential():
    reference = lambda u0, tau: lambda t: u0 * math.exp(-tau * t)
    direct = lambda u0, tau, dt: lambda t: u0 * (1 + tau * dt) ** -(t // dt)

    u0 = 1
    tau = 1
    tf = 5.0
    dt = 1e-3

    eps = dt * 1e0

    solve_step = Newton(
        1e-12,
        20,
        ExpAssembler(tau),
        ScalarLinearSolver(),
    )

    log = ListLog()
    timeloop = Timeloop(
        Timestepper(solve_step),
        UserList([dt]),
        callbacks=[log],
    )
    timeloop.run(u0, tf)
    f = reference(u0, tau)
    # f = direct(u0, tau, dt)
    rlog = log
    # rlog = rlog[-len(rlog)//1000:]
    # rlog = rlog[::len(rlog)//30]
    for v, t in rlog:
        # print(t, v, abs(v - f(t.time)))
        assert abs(v - f(t.time)) < eps
    # v, t = log[-1]
    # assert abs(v - f(t.time)) < eps


def test_abstract():
    u0 = 1
    tf = 1
    dt = 1e-3

    F = lambda u, t: 1
    log = ListLog()
    Timeloop(ExplicitStep(F), UserList([dt]), callbacks=[log]).run(u0, tf)
    u, t = log[-1]
    assert t.time > tf
    assert len(log) == t.iteration + 1
    assert abs(u - u0 - t.time) < 1e-10

    F = lambda u, t: u
    log = ListLog()
    Timeloop(ExplicitStep(F), UserList([dt]), callbacks=[log]).run(u0, tf)
    u, t = log[-1]
    assert t.time >= tf
    assert len(log) == t.iteration + 1
    assert abs(u - u0 * math.exp(t.time)) < dt * 2


if __name__ == "__main__":
    test_timeloop()
