import math

from globalgo.config import ConfigHelper, configure_timeloop, FixedPointInitializer
from globalgo.core import EventPool, SimpleFixedPointNorm
from globalgo.timestep import DynamicStep
from globalgo.callbacks import ListLog
from globalgo.shop.timing import Timer


class ExpFixedPoint:
    def __init__(self, tau):
        self.tau = tau

    def set_up(self, solution, dt, time):
        self.dt = dt
        self.time = time
        self.prev_solution = solution

    def evaluate(self, solution):
        sum(range(5000))  # fake work
        K = self.dt * self.tau
        new_solution = self.prev_solution - K * solution
        return new_solution, 1


class MyProblem:
    __function__ = ExpFixedPoint
    __residual_norm__ = SimpleFixedPointNorm

    def __init__(self, tau):
        self.log = ListLog(False)
        self.__function__ = self.__function__(tau)
        self.__residual_norm__ = self.__residual_norm__()

    def __events__(self, period=math.inf):
        return EventPool().add_recurring_time(period, self.log)

    class __callbacks__:
        def __init__(self, log=print):
            self.log = log

        def timestepper_make_step(self, solution, steps, tick):
            new_solution, step, statuses = yield
            self.log(
                "timestepper_make_step", solution, tick, new_solution, step, statuses
            )

        def function_evaluate(self, solution):
            new_solution, status = yield
            self.log("function_evaluate", solution, new_solution, status)

    def get_solver_initializer(self, config, namespaces, type_key):
        return FixedPointInitializer(self, config, namespaces, type_key)


def test():
    tau = 1
    u0 = 1

    H = ConfigHelper()
    log = ListLog(False)
    timer = Timer()
    pb = MyProblem(tau)

    config = dict(
        fixedpoint=dict(tolerance=1e-6, maxiter=200),
        timesteps=DynamicStep(0.1),
        events=[H.default(period=1)],
        callbacks=[H.default(log=log), timer],
    )
    loop = configure_timeloop(pb, config)
    solution, tick = loop.run(solution=u0, stop=5)

    print()
    print(timer.report_summary())
    print(timer.report_summary(inner=True))
    print(timer.report_inner_cumul())
