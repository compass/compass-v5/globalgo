from itertools import islice
from globalgo.timestep import DynamicStep


def test_DynamicStep():
    m = DynamicStep(
        initial_step=1,
        maximum_step=2**6,
        minimum_step=2**-6,
        increase_factor=2,
        decrease_factor=0.5,
        karma_threshold=None,
        karma_reset=True,
    )
    assert list(islice(m, 10)) == [1, 0.5, 0.25, 0.125, 0.0625, 0.03125, 0.015625]
    assert list(islice(m, 10)) == [0.015625]
    assert list(islice(m, 10)) == [0.015625]
    assert list(islice(m, 10)) == [0.015625]
    assert list(islice(m, 1)) == [0.015625]
    assert list(islice(m, 1)) == [0.03125]
    assert list(islice(m, 1)) == [0.0625]
    assert list(islice(m, 1)) == [0.125]
    assert list(islice(m, 1)) == [0.25]
    assert list(islice(m, 1)) == [0.5]
    assert list(islice(m, 1)) == [1]
    assert list(islice(m, 1)) == [2]
    assert list(islice(m, 1)) == [4]
    assert list(islice(m, 1)) == [8]
    assert list(islice(m, 5)) == [16, 8, 4, 2, 1]
    assert list(islice(m, 1)) == [2]
    assert list(islice(m, 1)) == [4]
    assert list(islice(m, 1)) == [8]
    assert list(islice(m, 1)) == [16]
    assert list(islice(m, 1)) == [32]
    assert list(islice(m, 1)) == [64]
    assert list(islice(m, 1)) == [64]
    assert list(islice(m, 1)) == [64]
    m.max_step = 2
    assert list(islice(m, 1)) == [2]
    assert list(islice(m, 1)) == [2]
    assert list(islice(m, 1)) == [2]
    m.max_step = None
    assert list(islice(m, 1)) == [64]

    m.step = 1
    m.max_step = 8
    assert list(islice(m, 1)) == [1]
    assert list(islice(m, 1)) == [2]
    assert list(islice(m, 1)) == [4]
    assert list(islice(m, 1)) == [8]
    assert list(islice(m, 1)) == [8]
    assert list(islice(m, 1)) == [8]
